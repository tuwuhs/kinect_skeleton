/* Skeleton Tracking using Kinect SDK
 * openFrameworks 0073, Kinect SDK 1.6
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#pragma once

#include "ofMain.h"
#include <Windows.h>
#include <Ole2.h>
#include <NuiApi.h>
#include "kinectSkeleton.h"

#define KINECT_WIDTH   (640)
#define KINECT_HEIGHT  (480)

class testApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		kinectSkeleton skeleton;
};
