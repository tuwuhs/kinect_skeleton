/* Skeleton Tracking using Kinect SDK
 * openFrameworks 0073, Kinect SDK 1.6
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	ofBackground(ofColor::black);

	if (!skeleton.setup()) {
		ofLog() << "Kinect init error";
	}
}

//--------------------------------------------------------------
void testApp::update(){
	skeleton.update();
}

//--------------------------------------------------------------
void testApp::draw(){
	skeleton.draw(0, 0);

	ofPushStyle();
	if (skeleton.isSkeletonFound()) {
		ofSetColor(ofColor::green);
		ofCircle(700, 100, 30);
	}
	if (skeleton.isPoseDetected(0)) {
		ofSetColor(ofColor::cyan);
		ofCircle(700, 200, 30);
	}
	if (skeleton.isPoseDetected(1)) {
		ofSetColor(ofColor::magenta);
		ofCircle(700, 300, 30);
	}
	ofPopStyle();

	ofDrawBitmapString("FPS: " + ofToString(ofGetFrameRate()), 10, 20);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}