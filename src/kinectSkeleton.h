/* Skeleton Tracking using Kinect SDK
 * openFrameworks 0073, Kinect SDK 1.6
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#pragma once

#include "ofMain.h"
#include <Windows.h>
#include <Ole2.h>
#include <NuiApi.h>

#define KINECT_WIDTH   (640)
#define KINECT_HEIGHT  (480)
#define KINECT_SKELETON_MAX_POSE  (2)


class kinectSkeleton {
public:
	bool setup(void);
	void update(void);
	void draw(int x, int y);
	bool isPoseDetected(int index);
	bool isSkeletonFound(void);

private:
	void acquire(void);
	void processSkeleton(void);
	void drawSkeleton(void);

	INuiSensor* kinect;  
	HANDLE colorStream;
	BYTE colorData[KINECT_WIDTH * KINECT_HEIGHT * 4];
	
	NUI_SKELETON_FRAME skeletonFrame;
	ofPoint skeletonColorPoints[NUI_SKELETON_POSITION_COUNT];
	ofPoint skeletonPoints[NUI_SKELETON_POSITION_COUNT];

	bool bNewSkeletonFrame;
	bool bNewColorFrame;
	bool bSkeletonFound;

	bool bPoseState[KINECT_SKELETON_MAX_POSE];

	ofTexture colorTexture;
};
