/* Skeleton Tracking using Kinect SDK
 * openFrameworks 0073, Kinect SDK 1.6
 *
 * (c) 2013 Tuwuh Sarwoprasojo
 * All rights reserved.
 */

#include "kinectSkeleton.h"

bool kinectSkeleton::setup(void) {
	int numSensors;
	if (NuiGetSensorCount(&numSensors) < 0 || numSensors < 1) return false;
	if (NuiCreateSensorByIndex(0, &kinect) < 0) return false;

	kinect->NuiInitialize(NUI_INITIALIZE_FLAG_USES_COLOR | NUI_INITIALIZE_FLAG_USES_SKELETON);
	kinect->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_COLOR,
		NUI_IMAGE_RESOLUTION_640x480,
		0,    // Image stream flags
		2,    // Number of frames to buffer
		NULL, // Event handle
		&colorStream);
	kinect->NuiSkeletonTrackingEnable(NULL, 0);

	bSkeletonFound = false;

	colorTexture.allocate(KINECT_WIDTH, KINECT_HEIGHT, GL_RGBA);

	return kinect;
}

void kinectSkeleton::draw(int x, int y) {
	colorTexture.draw(0, 0);
	drawSkeleton();
}

bool kinectSkeleton::isPoseDetected(int index) {
	if (index >= KINECT_SKELETON_MAX_POSE) {
		return false;
	}

	return bPoseState[index];
}

bool kinectSkeleton::isSkeletonFound(void) {
	return bSkeletonFound;
}

void kinectSkeleton::update(void) {
	acquire();
	if (bNewSkeletonFrame) {
		processSkeleton();
	}
	colorTexture.loadData(colorData, KINECT_WIDTH, KINECT_HEIGHT, GL_BGRA);
}

void kinectSkeleton::acquire(void) {
	HRESULT hr;
	NUI_IMAGE_FRAME imageFrame;

	// Get image stream data
	hr = kinect->NuiImageStreamGetNextFrame(colorStream, 0, &imageFrame);
	if (SUCCEEDED(hr)) {
		bNewColorFrame = true;

		INuiFrameTexture* texture = imageFrame.pFrameTexture;
		NUI_LOCKED_RECT lockedRect;
		texture->LockRect(0, &lockedRect, NULL, 0);

		if (lockedRect.Pitch != 0) {
			memcpy(colorData, lockedRect.pBits, lockedRect.size);
		}

		texture->UnlockRect(0);
		kinect->NuiImageStreamReleaseFrame(colorStream, &imageFrame);
	} else {
		bNewColorFrame = false;
	}

	// Get skeleton data
	hr = kinect->NuiSkeletonGetNextFrame(0, &skeletonFrame);
	if (SUCCEEDED(hr)) {
		bNewSkeletonFrame = true;
		kinect->NuiTransformSmooth(&skeletonFrame, NULL);
	} else {
		bNewSkeletonFrame = false;
	}
}

void kinectSkeleton::processSkeleton() {
	// Determine user and convert to color coordinates
	INuiCoordinateMapper* coordinateMapper;
	kinect->NuiGetCoordinateMapper(&coordinateMapper);
	bSkeletonFound = false;
	for (int i=0; i<NUI_SKELETON_COUNT; i++) {
		NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;
		if (trackingState == NUI_SKELETON_TRACKED) {
			for (int j=0; j<NUI_SKELETON_POSITION_COUNT; j++) {
				Vector4 pos = skeletonFrame.SkeletonData[i].SkeletonPositions[j];
				NUI_COLOR_IMAGE_POINT screenPos;
				skeletonPoints[j].x = pos.x / pos.w;
				skeletonPoints[j].y = pos.y / pos.w;
				skeletonPoints[j].z = pos.z / pos.w;
				coordinateMapper->MapSkeletonPointToColorPoint(
					&pos, NUI_IMAGE_TYPE_COLOR, NUI_IMAGE_RESOLUTION_640x480, &screenPos);
				skeletonColorPoints[j].x = screenPos.x;
				skeletonColorPoints[j].y = screenPos.y;
			}
			// Return on first skeleton found
			// Nice to have: priority tracking based on distance, etc...
			bSkeletonFound = true;
			break;
		}
	}

	// Pose detection
	if (bSkeletonFound) {
		bPoseState[0] = 
			((skeletonPoints[NUI_SKELETON_POSITION_HAND_RIGHT].y > skeletonPoints[NUI_SKELETON_POSITION_HEAD].y) &&
			 (skeletonPoints[NUI_SKELETON_POSITION_HAND_LEFT].y > skeletonPoints[NUI_SKELETON_POSITION_HEAD].y));
		bPoseState[1] =
			((skeletonPoints[NUI_SKELETON_POSITION_SHOULDER_LEFT].z - skeletonPoints[NUI_SKELETON_POSITION_HAND_LEFT].z) > 0.5);
	} else {
		bPoseState[0] = bPoseState[1] = false;
	}
}

void drawBone(ofPoint screenPoints[], NUI_SKELETON_POSITION_INDEX from, NUI_SKELETON_POSITION_INDEX to) {
	ofLine(screenPoints[from], screenPoints[to]);
}

void kinectSkeleton::drawSkeleton() {
	if (!bSkeletonFound) {
		return;
	}

	ofDrawBitmapString(
		ofToString(skeletonPoints[NUI_SKELETON_POSITION_HAND_LEFT].z),
		650, 400);
	ofDrawBitmapString(
		ofToString(skeletonPoints[NUI_SKELETON_POSITION_SHOULDER_LEFT].z),
		650, 430);

	// Draw bones
	ofPushStyle();
	ofSetLineWidth(3);
	ofSetColor(ofColor::yellow);

	// Render Torso
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_HEAD, NUI_SKELETON_POSITION_SHOULDER_CENTER);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_LEFT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_RIGHT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SPINE);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_SPINE, NUI_SKELETON_POSITION_HIP_CENTER);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_LEFT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_RIGHT);
	// Left Arm	  
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_SHOULDER_LEFT, NUI_SKELETON_POSITION_ELBOW_LEFT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_ELBOW_LEFT, NUI_SKELETON_POSITION_WRIST_LEFT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_WRIST_LEFT, NUI_SKELETON_POSITION_HAND_LEFT);
	// Right Arm 
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_SHOULDER_RIGHT, NUI_SKELETON_POSITION_ELBOW_RIGHT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_ELBOW_RIGHT, NUI_SKELETON_POSITION_WRIST_RIGHT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_WRIST_RIGHT, NUI_SKELETON_POSITION_HAND_RIGHT);
	// Left Leg	
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_HIP_LEFT, NUI_SKELETON_POSITION_KNEE_LEFT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_KNEE_LEFT, NUI_SKELETON_POSITION_ANKLE_LEFT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_ANKLE_LEFT, NUI_SKELETON_POSITION_FOOT_LEFT);
	// Right Leg   
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_HIP_RIGHT, NUI_SKELETON_POSITION_KNEE_RIGHT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_KNEE_RIGHT, NUI_SKELETON_POSITION_ANKLE_RIGHT);
	drawBone(skeletonColorPoints, NUI_SKELETON_POSITION_ANKLE_RIGHT, NUI_SKELETON_POSITION_FOOT_RIGHT);
	ofPopStyle();

	// Draw joints
	ofPushStyle();
	ofSetColor(ofColor::red);
	for (int j=0; j<NUI_SKELETON_POSITION_COUNT; j++) {
		ofCircle(skeletonColorPoints[j], 5);
	}
	ofPopStyle();
}